﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MovingObject : MonoBehaviour
{
    private const int MAX_OBJECT_POSITION = 26;
    private const int MIN_OBJECT_POSITION = -26;

    [SerializeField] private float speed;
    
    public GameObject movingObject;

    private void Update()
    {
        if (movingObject.tag == "Bus")
        {
            
            transform.Translate(Vector3.right * speed * Time.deltaTime);
        }
        else
        {
            transform.Translate(Vector3.back * speed * Time.deltaTime);
        }
        
        DeactivateObject();
    }

    private void DeactivateObject()
    {
        float zPosition = transform.position.z;
        if (zPosition > MAX_OBJECT_POSITION || zPosition < MIN_OBJECT_POSITION)
        {
            movingObject.SetActive(false);
        }
    }
}
