﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillPlayer : MonoBehaviour
{
    private PlayerController player;
    private void Start()
    {
        player = PlayerController.player;
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.GetComponent<PlayerController>() != null)
        {
            Destroy(collision.gameObject);
            player.Lose();
        }
    }
}
